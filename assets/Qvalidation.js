QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { re(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(re(2), 2, ' positive number');
    assert.strictEqual(re(-50), -50, ' negative number');
});
